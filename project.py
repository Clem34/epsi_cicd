import math


def is_prime(nombre):
    if nombre < 0:
        return 'Negative numbers are not allowed'

    if nombre <= 1:
        return False

    if nombre == 2:
        return True

    if nombre % 2 == 0:
        return False

    for i in range(2, int(math.sqrt(nombre)) + 1):
        if nombre % i == 0:
            return False
    return True


def cubic(cote):
    return cote * cote * cote


def say_hello(name):
    return "Hello, " + name

say_hello("world !")
